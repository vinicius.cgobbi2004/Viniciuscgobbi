# Eae meu nome é Vinicius 🙂️

----
![Visitantes](https://visitor-badge.laobi.icu/badge?page_id=Viniciuscgobbi.Viniciuscgobbi)
----

<div align="center">
  <a href="https://github.com/Viniciuscgobbi">
  <img height="150em" src="https://github-readme-stats.vercel.app/api?username=Viniciuscgobbi&show_icons=true&theme=nord&include_all_commits=true&count_private=true"/>
  <img height="150em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=viniciuscgobbi&layout=compact&langs_count=7&theme=nord"/>
</div>

<div style="display: inline_block"><br>
  <img align="center" alt="Vini-Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="Rafa-Python" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  <img align="center" alt="Rafa-Python" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bash/bash-original.svg">
  <img align="right" alt="Vini-pic" height="150" style="border-radius:50px;" src="https://cdn.discordapp.com/attachments/806642907263139850/917209212969418802/profile.jpg">
</div>

  ##

  ### 🖥️ Redes sociais:
  
<div> 
  <a href="https://www.youtube.com/channel/UClHEUWReZI_uxuXs0J7NEpQ" target="_blank"><img src="https://img.shields.io/badge/YouTube-FF0000?style=for-the-badge&logo=youtube&logoColor=white" target="_blank"></a>
  <a href="https://www.instagram.com/vinicius_cavati/" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
 	<a href="https://www.twitch.tv/androwinbr" target="_blank"><img src="https://img.shields.io/badge/Twitch-9146FF?style=for-the-badge&logo=twitch&logoColor=white" target="_blank"></a>
  <a href = "mailto:vinicius.cgobbi2004@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
 
  ##
  
  ### 🏆️ Troféus do Github
  
  [![trophy](https://github-profile-trophy.vercel.app/?username=Viniciuscgobbi&theme=nord)](https://github.com/ryo-ma/github-profile-trophy)
 
  ##
  
  ![Snake animation](https://github.com/Viniciuscgobbi/Viniciuscgobbi/blob/output/github-contribution-grid-snake.svg)

</div>
